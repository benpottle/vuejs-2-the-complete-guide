new Vue({
  el: '#exercise',
  data: {
    attachHighlight: false,
    height: 200,
    width: 500,
    userClass: 'red'
  },
  methods: {
    startEffect: function() {
      var vm = this;
      setInterval(function() {
          vm.attachHighlight = !vm.attachHighlight;
      }, 2000);
    }
  },
    computed: {
      divClasses: function() {
        return {
          highlight: this.attachHighlight,
          shrink: !this.attachHighlight
        };
      }
    }
});
